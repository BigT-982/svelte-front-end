import baseUrl from './Common.js';
import { SendPostRequest } from '../Services/HTTPService.js';
export async function PostPrimarySelection(data) {
    return await SendPostRequest(`${baseUrl}/api/selections`, data);
}

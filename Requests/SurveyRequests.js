import baseUrl from './Common.js';
import { SendGetRequest } from '../Services/HTTPService.js';
export async function GetSurveyTypeById(id) {
    return await SendGetRequest(`${baseUrl}/api/surveytypes/${id}`);
}
export async function GetSurveyQuestionsBySurveyTypeId(id) {
    return await SendGetRequest(`${baseUrl}/api/surveytypes/${id}/questions`);
}
export async function GetSurveyAreasBySurveyTypeId(id) {
    return await SendGetRequest(`${baseUrl}/api/surveytypes/${id}/areas`);
}
export async function GetAllSurveyTypes(){
    return await SendGetRequest(`${baseUrl}/api/surveytypes`);
}
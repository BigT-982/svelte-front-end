import baseUrl from './Common.js';

import { SendGetRequest } from '../Services/HTTPService.js';
export async function GetImpactScoresForSurveyDateId(id) {

    let res = await SendGetRequest(`${baseUrl}/api/impactScores/surveyDate/${id}`);
    return res;
}
export async function GetWealthScoresForSurveyDateId(id) {
    return await SendGetRequest(`${baseUrl}/api/wealthScores/surveyDate/${id}`);
}
export async function GetTotalScoreForSurveyDateId(id) {
    return await SendGetRequest(`${baseUrl}/api/totalScore/${id}`);
}
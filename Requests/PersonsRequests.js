import baseUrl from './Common.js';
import { SendPostRequest, SendPatchRequest,SendGetRequest } from '../Services/HTTPService.js';
export async function PostPerson(data) {

    return await SendPostRequest(`${baseUrl}/api/persons`, data);
}

export async function AuthorizePerson(data) {
    return await SendPostRequest(`${baseUrl}/api/persons/auth`, data);
}

export async function GetPerson(data) {
    return await SendPostRequest(`${baseUrl}/api/persons/email`, data);
}

export async function UpdatePerson(data) {
    let updateURL = `${baseUrl}/api/persons/`+ data.id
    return await SendPatchRequest(updateURL, data);
}

export async function GetPersonByEmail(data) {
    let URL = `${baseUrl}/api/persons/personsbyemail/`+ data
    return await SendGetRequest(URL);
}

export async function PostAppPermission(data) {

    return await SendPostRequest(`${baseUrl}/api/persons/app_permissions`, data);
}


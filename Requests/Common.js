const productionUrl = `https://be.network.impatu.com`; 
const developmentUrl = `https://be.development.impatu.com`; 
const localUrl = "http://localhost:3000"

let baseUrl;
const environment = import.meta.env.VITE_NODE_ENV

if(environment==="production"){
    baseUrl=productionUrl;
  } else if (environment === 'development') {
    baseUrl=developmentUrl;
  } else if (environment === 'local') {
    baseUrl=localUrl;
  }
else {
  // Throw an error for unrecognized environment
  throw new Error(`Unrecognized VITE_NODE_ENV: ${environment}`);
}

export default baseUrl;
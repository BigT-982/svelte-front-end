import baseUrl from './Common.js';
import { SendPostRequest } from '../Services/HTTPService.js';
import { SendPatchRequest } from '../Services/HTTPService.js';
export async function PostSurveyDate(data) {
    return await SendPostRequest(`${baseUrl}/api/surveydates`, data);
}
export async function PatchSurveyDate(data) {
    return await SendPatchRequest(`${baseUrl}/api/surveydates/${data.id}`, data);
}


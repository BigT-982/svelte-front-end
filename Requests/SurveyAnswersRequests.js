import baseUrl from './Common.js';
import { SendPostRequest } from '../Services/HTTPService.js';
export async function PostSurveyAnswers(data) {
    return await SendPostRequest(`${baseUrl}/api/answers`, data);
}
export async function PostAllSurveyAnswers(data) {
    return await SendPostRequest(`${baseUrl}/api/answers/all`, data);
}

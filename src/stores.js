import { writable } from 'svelte/store';

// Create surveyStore - note this is not an object.. it has its own subscribe, set and update methods.
export const surveyStore = writable();

export const primaryAreaChosen = writable();

export const secondaryAreaChosen = writable();

export const selectionsStored = writable();

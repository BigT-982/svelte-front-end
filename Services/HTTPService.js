const username = 'terence';
const password = '?3Qx4Mf!=9gQB9M5';

export async function SendGetRequest(url) {
    try {
        let response = await fetch(url,
            {
                method: 'GET',
                headers: {
                    "Content-Type": "text/plain",
                    'Authorization': 'Basic ' + btoa(username + ":" + password),
                }
            });
        let json = await response.json();
        return json;
    }
    catch (err) {
        console.log(err);
        return null;
    }
}
export async function SendPostRequest(url, data) {

    try {
        let response = await fetch(url,
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Basic ' + btoa(username + ":" + password),
                },
                body: JSON.stringify(data)
            });
        let json = await response.json();
        return json;
    }
    catch (err) {
        console.log(err);
        return null;
    }
}

export async function SendPatchRequest(url, data) {
    try {
        let response = await fetch(url,
            {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Basic ' + btoa(username + ":" + password),
                },
                body: JSON.stringify(data)
            });
        let json = await response.json();
        return json;
    }
    catch (err) {
        console.log(err);
        return null;
    }
}
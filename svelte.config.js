import nadapter from '@sveltejs/adapter-node';
import vadapter from '@sveltejs/adapter-vercel';

import sadapter from '@sveltejs/adapter-static';
let adapter;
let spaMode = false;
let vercelMode = true;
if (spaMode) {
	adapter = sadapter({
		fallback: '200.html'
	});
}
else if (vercelMode){
	adapter = vadapter({
		out: 'build'
	});
}
else {
	adapter = nadapter({
		out: 'build'
	});
}
export default {
	kit: {
		adapter: adapter
	}
};
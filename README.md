## Deploying

Run following command to deploy production build of this app

```bash
npm start
```

The above command will create a production build of the app in 'build' folder then run it on thr process.env.PORT defined port number.